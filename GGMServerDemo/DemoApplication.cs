﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using GGMServer;
using GGMServer.DI.Attribute;
using GGMServer.Router;
using GGMServer.Server.Attribute;
using GGMServer.Server.TCP;
using GGMServer.Server.TCP.Packet;

namespace GGMServerDemo
{
    [Managed]
    public class TestManagedClass1
    {

    }

    [Managed]
    public class TestManagedClass2
    {

    }

    [Managed]
    public class TestManagedClass3
    {

    }

    [Managed]
    public class TestManagedClass4
    {

    }

    class DemoApplication
    {
        static void Main(string[] args)
        {
            GGMServerApplication.Run(typeof(DemoApplication), args);
            //TODO: 테스트를 위한 Spinning
            while (true){}
        }
    }

    [Server]
    public class DemoServer : TCPServer
    {
        [AutoWired]
        public DemoServer(DemoRouter router)
        {
            Router = router;
            EndPoint = new IPEndPoint(IPAddress.Any, 9006);
            Task.Run(() => Run());
        }

        public override Router Router { get; set; }
        protected override void OnClientAccepted(Client client)
        {
            Console.WriteLine($"Accept : {IPAddress.Parse(((IPEndPoint)client.Socket.RemoteEndPoint).Address.ToString())}");
        }

        protected override void OnReceived(Client client, Packet receivePacket)
        {
//            Console.WriteLine($"Receive - {client.ID} : {IPAddress.Parse(((IPEndPoint)client.Socket.RemoteEndPoint).Address.ToString())} - {receivePacket.Route}");
        }
    }
}
