﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Reflection.Emit;
using GGMServer.DI;
using GGMServer.DI.Attribute;
using GGMServer.Router;
using GGMServer.Router.Attribute;
using GGMServer.Serialize;
using GGMServer.Server.TCP;
using GGMServer.Server.TCP.Packet;

namespace GGMServerDemo
{
    public class Serializer : ISerializer
    {
        public byte[] Serialize<T>(T target)
        {
            return new byte[1024];
        }

        public T Deserialize<T>(byte[] data)
        {
            return default(T);
        }
    }

    public class TestSerializeObject
    {
        
    }

    //TODO: 추후 소켓의 데이터를 읽은 뒤 헤더를 이용해서 Router에 보낸다, Router은 자신 Controller들의  Routed Method들을 기억했다가 해당하는 RoutedMethod 를 실행한다.
    [Router]
    public class DemoRouter : Router
    {
		private UserController _userController;
        private MapController _mapController;
        private ConfigurationController _configurationController;

        [AutoWired]
        public DemoRouter(ApplicationContext context, UserController userController, MapController mapController, ConfigurationController configurationController) : base(context, new Serializer())
        {
            Context = context;
            _userController = userController;
            _mapController = mapController;
            _configurationController = configurationController;
            
            //Controller들이 등록된다. 패킷이 들어왔을떄 Header의 Route 값에 따라 매핑된 Controller를 실행함.
            RegisterController(_userController);
            //RegisterController(typeof(_mapController));
            //RegisterController(typeof(_configurationController));

//            const int LOOP_COUNT = 100000000;
//            var test = new Test();
//            var delegateSW = Stopwatch.StartNew();
//            for (int i = 0; i < LOOP_COUNT; i++)
//            {
//                AA();
//            }
//            delegateSW.Stop();
//            
//            var test1 = new Test();
//            Action lambda = Delegate.CreateDelegate(typeof(Action), this, this.GetType().GetMethod("AA")) as Action;
//            var labmdaSW = Stopwatch.StartNew();
//            for (int i = 0; i < LOOP_COUNT; i++)
//            {
//                lambda();
//            }
//            labmdaSW.Stop();
//
//            string message = $"{delegateSW.ElapsedMilliseconds} | {labmdaSW.ElapsedMilliseconds}";
//            Console.WriteLine(message);
        }

        public void AA()
        {
            var aa = new Test();
        }
    }

    public class Test { public int Value { get; set; } }

    [Controller]
    public class UserController
    {
        [AutoWired]
        public UserController(UserService service, MapController controller)
        {
            
        }

        [Route(0)]
        public Packet UserInfo(UserService userService, TCPServer.Client client, [Packet]Packet packet)
        {
            //userService가 자동주입된다.
            //멤버로 하지 않는 이유는 동기적 처리때문
            //어 근대 그래도 문제있네
            //흐응으으으응으으으음
//            throw new NotImplementedException();
            return packet;
        }
    }

    [Managed]
    public class UserService
    {
        
    }

    [Controller]
    public class MapController
    {
        
    }

    [Controller]
    public class ConfigurationController
    {
        
    }
}