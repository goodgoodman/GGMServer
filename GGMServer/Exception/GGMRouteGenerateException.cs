﻿using System.Diagnostics;
using GGMServer.Util;

namespace GGMServer.Exception
{
    public enum RouteGenerateExceptionType
    {
        ParameterIsPlain, // 파라미터는 반드시 소켓, 패킷이거나 Managed여야 합니다.
        DefaultSerializerIsNull,// Serializer가 지정되지 않음.
    }

    public class GGMRouteGenerateException : System.Exception
    {
        private const string EXCEPTION_MESSAGE_TEMPLATE = "RouteGenerate에 실패하였습니다. : {0}";

        public GGMRouteGenerateException(RouteGenerateExceptionType type) : base(EXCEPTION_MESSAGE_TEMPLATE.SetValue(GetMessage(type)))
        {
        }

        private static string GetMessage(RouteGenerateExceptionType type)
        {
            string resultMessage = "";
            switch (type)
            {
                case RouteGenerateExceptionType.ParameterIsPlain:
                    resultMessage = "파라미터는 반드시 소켓, 패킷이거나 Meanaged여야 합니다.";
                    break;

                case RouteGenerateExceptionType.DefaultSerializerIsNull:
                    resultMessage = "Route의 Serialize의 값이 지정되지 않았습니다.";
                    break;
            }
            return resultMessage;
        }
    }
}