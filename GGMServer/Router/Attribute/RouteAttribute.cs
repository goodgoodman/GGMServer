﻿using System;

namespace GGMServer.Router.Attribute
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RouteAttribute : System.Attribute
    {
        public RouteAttribute(ushort routeValue)
        {
            Value = routeValue;
        }

        public ushort Value { get; private set; }
    }
}