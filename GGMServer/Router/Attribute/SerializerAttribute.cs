﻿using System;

namespace GGMServer.Router.Attribute
{
    public class SerializerAttribute : System.Attribute
    {
        public SerializerAttribute(Type type)
        {
            Type = type;
        }

        public Type Type { get; set; }
    }
}