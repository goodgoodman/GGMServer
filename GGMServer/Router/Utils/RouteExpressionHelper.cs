﻿using System.Linq.Expressions;
using System.Net.Sockets;
using System.Reflection;
using GGMServer.Server.TCP.Packet;

namespace GGMServer.Router.Utils
{
    public static class RouteExpressionHelper
    {
        public static BinaryExpression Assign(this ParameterExpression self, Expression target)
        {
            return Expression.Assign(self, target);
        }

        public static BinaryExpression Assign(this ParameterExpression self, object target)
        {
            return Assign(self, Expression.Constant(target));
        }

        public static bool IsRouteParameterExpression(this ParameterExpression self)
        {
            return self.Type == typeof(Socket) || self.Type == typeof(Packet);
        }
    }
}