﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Reflection;
using GGMServer.DI;
using GGMServer.DI.Attribute;
using GGMServer.Router.Attribute;
using GGMServer.Serialize;
using GGMServer.Server.TCP;
using GGMServer.Server.TCP.Packet;

namespace GGMServer.Router
{
    public abstract class Router
    {
        protected Router(ApplicationContext context, ISerializer defaultSerializer)
        {
            Context = context;
            DefaultSerializer = defaultSerializer;
            if (DefaultSerializer != null)
                SerializerDictionary[DefaultSerializer.GetType()] = DefaultSerializer;
        }

        public delegate Packet RouteDelegate(TCPServer.Client client, Packet packet);
        public ApplicationContext Context { get; protected set; }
        public ISerializer DefaultSerializer { get; set; }
        public Dictionary<Type, ISerializer> SerializerDictionary { get; set; } = new Dictionary<Type, ISerializer>();
        public Dictionary<ushort, RouteDelegate> RouteDelegateDictionary { get; set; } = new Dictionary<ushort, RouteDelegate>();

        public void RegisterController(object controller)
        {
            var controllerType = controller.GetType();
            var routeMethodInfos = controllerType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var routeMethodInfo in routeMethodInfos)
            {
                var routeAttribute = routeMethodInfo.GetCustomAttribute<RouteAttribute>(false);
                if (routeAttribute == null)
                    continue;
                RouteDelegateDictionary[routeAttribute.Value] = RouteMethodGenerator.Generate(this, controller, routeMethodInfo);
            }
        }

        public Packet Route(ushort route, TCPServer.Client client, Packet packet)
        {
            return RouteDelegateDictionary[route](client, packet);
        }
    }
}