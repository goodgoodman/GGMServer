﻿using System.Collections.Generic;
using System.Linq.Expressions;
using GGMServer.Router.Utils;

namespace GGMServer.Router
{
    /// <summary>
    ///     Expression 사용 시 BlockExpression을 쉽게 만들기 위한 클래스입니다.
    /// </summary>
    public class ExpressionBlockMaker
    {
        public List<ParameterExpression> Parameters { get; set; } = new List<ParameterExpression>();
        public List<Expression> Assigns { get; set; } = new List<Expression>();

        public void DeclearParameter(ParameterExpression parameterExpression)
        {
            Parameters.Add(parameterExpression);
        }

        public void Assign(Expression assignExpression)
        {
            Assigns.Add(assignExpression);
        }

        public void DeclearAndAssign(ParameterExpression parameterExpression, object @object)
        {
            DeclearParameter(parameterExpression);
            Assign(parameterExpression.Assign(@object));
        }

        public void DeclearAndAssign(ParameterExpression parameterExpression, Expression rightExpression)
        {
            DeclearParameter(parameterExpression);
            Assign(parameterExpression.Assign(rightExpression));
        }

        public BlockExpression Make()
        {
            return Expression.Block(Parameters, Assigns);
        }
    }
}