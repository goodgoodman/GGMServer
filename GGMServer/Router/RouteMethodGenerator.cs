﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Reflection;
using GGMServer.DI;
using GGMServer.DI.Attribute;
using GGMServer.Exception;
using GGMServer.Router.Attribute;
using GGMServer.Router.Utils;
using GGMServer.Serialize;
using GGMServer.Server.TCP;
using GGMServer.Server.TCP.Packet;

namespace GGMServer.Router
{
    public static class RouteMethodGenerator
    {
        public static Router.RouteDelegate Generate(Router router, object controller, MethodInfo routeMethodInfo)
        {
            //Packet Attriube가 달린 인자의 Type에 따라 아래 셋 중 하나의 코드가 작성됨.
            /////////// public Packet UserInfo(UserService userService, Socket socket, [Packet]Packet packet)
            // .Lambda #Lambda1<GGMServer.Router.Router+RouteDelegate>(System.Net.Sockets.Socket $Socket, GGMServer.Server.TCP.Packet.Packet $Packet) 
            // {
            //     .Block(GGMServerDemo.UserController $controller, GGMServerDemo.UserService $userService, System.Net.Sockets.Socket $socket, GGMServer.Server.TCP.Packet.Packet $packet)
            //     {
            //         $controller = .Constant<GGMServerDemo.UserController>(GGMServerDemo.UserController);  
            //         $userService = .Constant<GGMServerDemo.UserService>(GGMServerDemo.UserService);
            //         $socket = $Socket;
            //         $packet = $Packet;
            //          .Call $controller.UserInfo($userService, $socket, $packet)
            //     }
            // }
            ///////////

            /////////// public Packet UserInfo(UserService userService, Socket socket, [Packet]byte[] packet)
            // .Lambda #Lambda1<GGMServer.Router.Router+RouteDelegate>(System.Net.Sockets.Socket $Socket, GGMServer.Server.TCP.Packet.Packet $Packet) 
            // {
            //     .Block(GGMServerDemo.UserController $controller, GGMServerDemo.UserService $userService, System.Net.Sockets.Socket $socket, System.Byte[] $packet)
            //     {
            //         $controller = .Constant<GGMServerDemo.UserController>(GGMServerDemo.UserController);
            //         $userService = .Constant<GGMServerDemo.UserService>(GGMServerDemo.UserService);
            //         $socket = $Socket;
            //         $packet = $Packet.Body;
            //         .Call $controller.UserInfo($userService, $socket, $packet)
            //     }
            // }
            ///////////

            /////////// public Packet UserInfo(UserService userService, Socket socket, [Packet]TestSerializeObject packet)
            //  .Lambda #Lambda1<GGMServer.Router.Router+RouteDelegate>(System.Net.Sockets.Socket $Socket, GGMServer.Server.TCP.Packet.Packet $Packet) 
            // {
            //     .Block(GGMServerDemo.UserController $controller, GGMServerDemo.UserService $userService, System.Net.Sockets.Socket $socket, GGMServerDemo.TestSerializeObject $packet, GGMServerDemo.Serializer $serializer)
            //     {
            //         $controller = .Constant<GGMServerDemo.UserController>(GGMServerDemo.UserController);
            //         $userService = .Constant<GGMServerDemo.UserService>(GGMServerDemo.UserService);
            //         $socket = $Socket;
            //         $serializer = .Constant<GGMServerDemo.Serializer>(GGMServerDemo.Serializer);
            //         $packet = .Call $serializer.Deserialize($Packet.Body);
            //         .Call $controller.UserInfo($userService, $socket, $packet);
            //     }
            // }
            ///////////

            ParameterInfo[] parameterInfos = routeMethodInfo?.GetParameters();
            Debug.Assert(parameterInfos != null, "parameterInfo들을 얻어올 수 없습니다.");

            ParameterExpression packetParameterForLambda = Expression.Parameter(typeof(Packet), nameof(Packet));
            ParameterExpression clientParameterForLambda = Expression.Parameter(typeof(TCPServer.Client), nameof(TCPServer.Client));

            var expressionBlockMaker = new ExpressionBlockMaker();

            ParameterExpression controllerParameter = Expression.Parameter(controller.GetType(), nameof(controller));
            expressionBlockMaker.DeclearAndAssign(controllerParameter, controller);

            var routeMethodParameterExpressions = new List<ParameterExpression>(parameterInfos.Length);
            foreach (var parameterInfo in parameterInfos)
            {
                ParameterExpression parameterExpression = Expression.Parameter(parameterInfo.ParameterType, parameterInfo.Name);
                routeMethodParameterExpressions.Add(parameterExpression);
                if (parameterInfo.IsDefined(typeof(PacketAttribute)))
                    SetPacketExpression(expressionBlockMaker, parameterExpression, packetParameterForLambda, router.DefaultSerializer);
                else if (parameterInfo.ParameterType == typeof(TCPServer.Client))
                    expressionBlockMaker.DeclearAndAssign(parameterExpression, clientParameterForLambda);
                else if (parameterInfo.ParameterType.IsDefined(typeof(ManagedAttribute), true))
                    expressionBlockMaker.DeclearAndAssign(parameterExpression, router.Context.GetManagedObject(parameterInfo.ParameterType));
                else
                    throw new GGMRouteGenerateException(RouteGenerateExceptionType.ParameterIsPlain);
            }
            expressionBlockMaker.Assign(Expression.Call(controllerParameter, routeMethodInfo, routeMethodParameterExpressions)); //TODO: 이제 와서 보니까 의미적으로 좀 아닐수도?

            var lambdaBlock = expressionBlockMaker.Make();
            var lambdaExpression = Expression.Lambda<Router.RouteDelegate>(lambdaBlock, clientParameterForLambda, packetParameterForLambda);

            return lambdaExpression.Compile();
        }

        private static void SetPacketExpression(ExpressionBlockMaker expressionBlockMaker, ParameterExpression packetParameterExpression, ParameterExpression packetParameterForLambda, ISerializer serializer)
        {
            if(serializer == null)
                throw new GGMRouteGenerateException(RouteGenerateExceptionType.DefaultSerializerIsNull);

            expressionBlockMaker.DeclearParameter(packetParameterExpression);
            MemberExpression packetBodyParameter = Expression.Field(packetParameterForLambda, "Body");
            Type expressionType = packetParameterExpression.Type;
            if (expressionType == typeof(Packet))
                expressionBlockMaker.Assign(packetParameterExpression.Assign(packetParameterForLambda));
            else if (expressionType == typeof(byte[]))
                expressionBlockMaker.Assign(packetParameterExpression.Assign(packetBodyParameter));
            else
            {
                ParameterExpression serializerParameter = Expression.Parameter(serializer.GetType(), nameof(serializer));
                expressionBlockMaker.DeclearAndAssign(serializerParameter, serializer);

                MethodInfo genericDeserializeMethodInfo = typeof(ISerializer).GetMethod("Deserialize");
                MethodInfo deserializeMethodInfo = genericDeserializeMethodInfo.MakeGenericMethod(expressionType);
                expressionBlockMaker.Assign(packetParameterExpression.Assign(Expression.Call(serializerParameter, deserializeMethodInfo, packetBodyParameter)));
            }
        }
    }
}