﻿namespace GGMServer.Serialize
{
    public interface ISerializer
    {
        byte[] Serialize<T>(T target);
        T Deserialize<T>(byte[] data);
    }

    public class MySerializer : ISerializer
    {
        public byte[] Serialize<T>(T target)
        {
            throw new System.NotImplementedException();
        }

        public T Deserialize<T>(byte[] data)
        {
            throw new System.NotImplementedException();
        }
    }
}