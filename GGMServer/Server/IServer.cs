﻿using System.Threading.Tasks;

namespace GGMServer.Server
{
    public interface IServer
    {
        void Run();
    }
}