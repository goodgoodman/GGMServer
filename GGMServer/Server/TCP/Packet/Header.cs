﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GGMServer.Serialize;

namespace GGMServer.Server.TCP.Packet
{
    public class Header
    {
        public int Size { get; set; }
        public byte[] Data { get; set; }
    }
}
