﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using GGMServer.DI.Attribute;
using GGMServer.Util;

namespace GGMServer.Server.TCP
{
    public abstract class TCPServer : IServer
    {
        public abstract Router.Router Router { get; set; }

        public Socket ServerSocket { get; private set; }
        public AddressFamily AddressFamily { get; protected set; } = AddressFamily.InterNetwork;
        public SocketType SocketType { get; protected set; } = SocketType.Stream;
        public IPEndPoint EndPoint { get; protected set; }
        public bool IsClose { get; private set; }

        public int BackLog { get; protected set; } = 1024;

        //TODO: 절대 그냥 호출해선 안된다. 다른 스레드에 올려서 해야할듯.
        public void Run()
        {
            ServerSocket = new Socket(AddressFamily, SocketType, ProtocolType.Tcp);

            //TODO: 추후 GGMException
            if (EndPoint == null)
                throw new System.Exception();

            ServerSocket.Bind(EndPoint);
            ServerSocket.Listen(BackLog);

            AcceptClient();
        }

        public async void AcceptClient()
        {
            while (!IsClose)
            {
                Socket clientSocket = await ServerSocket.AcceptFromAsync();
                var client = new Client(clientSocket);
                OnClientAccepted(client);
                RunClient(client);
            }
        }

        //TODO: 네이밍과 포함 클래스 수정.
        public async void RunClient(Client client)
        {
            while (client.IsConnected)
            {
                var readData = await client.ReceiveAsync(Packet.Packet.HEADER_SIZE);
                var bodyLength = BitConverter.ToUInt16(readData, 0);
                var route = BitConverter.ToUInt16(readData, 2);
                //TODO: Body를 읽을때는 이미 스트림에 데이터가 와 있다는 것이므로 동기적으로 가져와도 되지 않을까 고려가 필요함.
                var body = await client.ReceiveAsync(bodyLength);
                var requestPacket = new Packet.Packet(bodyLength, route, body);
                OnReceived(client, requestPacket);
                Packet.Packet returnPacket = Router.Route(requestPacket.Route, client, requestPacket);
                if (returnPacket != null)
                    client.SendAsync(returnPacket.ToByteSegements());
            }
        }

        protected abstract void OnClientAccepted(Client client);
        protected abstract void OnReceived(Client client, Packet.Packet receivePacket);
//        protected abstract void OnSended(Client client, Packet.Packet sendPacket);

        public virtual void Close()
        {
            ServerSocket.Close();
            IsClose = true;
        }

        public class Client
        {
            public const uint DEFAULT_BUFFER_SIZE = 1024;

            public Client(Socket socket) : this(socket, DEFAULT_BUFFER_SIZE) { }

            public Client(Socket socket, uint bufferSize)
            {
                ID = UniqueIDGenerator.GenerateID();
                Socket = socket;
                SetBuffer(bufferSize);
            }

            public uint ID { get; private set; }
            public Socket Socket { get; private set; }
            public bool IsConnected { get { return Socket.Connected; } }
            public uint BufferSize { get; private set; }
            private byte[] _readBuffer;

            public void SetBuffer(uint bufferSize)
            {
                BufferSize = bufferSize;
                _readBuffer = new byte[BufferSize];
            }

            [Obsolete("레거시 코드입니다. 추후삭제될 수 있습니다.")]
            public async Task<byte[]> ReceiveAsync()
            {
                int readSize = await Socket.ReceiveFromAsync(_readBuffer);
                var bytes = new byte[readSize];
                System.Buffer.BlockCopy(_readBuffer, 0, bytes, 0, readSize);
                return bytes;
            }


            public async Task<byte[]> ReceiveAsync(int size)
            {
                var bytes = new byte[size];
                int offset = 0;
                //TODO: size가 Buffer를 넘어가는 경우의 동작 확인이 필요함.
                while (offset != size)
                {
                    int readSize = await Socket.ReceiveFromAsync(_readBuffer, size - offset);
                    System.Buffer.BlockCopy(_readBuffer, 0, bytes, offset, readSize);
                    offset += readSize;
                }
                return bytes;
            }

            public async Task SendAsync(byte[] writeBytes)
            {
                await Socket.SendFromAsync(writeBytes);
            }

            public async Task SendAsync(IList<ArraySegment<byte>> writeBytes)
            {
                await Socket.SendFromAsync(writeBytes);
            }

            public static class UniqueIDGenerator
            {
                private static int _count = 0;

                public static uint GenerateID()
                {
                    return (uint)Interlocked.Increment(ref _count);
                }
            }
        }
    }
}