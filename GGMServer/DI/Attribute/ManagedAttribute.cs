﻿using System;

namespace GGMServer.DI.Attribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ManagedAttribute : System.Attribute
    {
        
    }
}