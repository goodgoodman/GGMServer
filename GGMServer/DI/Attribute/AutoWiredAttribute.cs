﻿using System;

namespace GGMServer.DI.Attribute
{
    [AttributeUsage(AttributeTargets.Constructor)]
    public class AutoWiredAttribute : System.Attribute
    {
        
    }
}