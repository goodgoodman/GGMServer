﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GGMServer.DI.Attribute;

namespace GGMServer.DI
{
    //TODO: 추후 인터페이스 분리될 수도 있음, BeanFactory 역할도함.
    public class ApplicationContext
    {
        public ApplicationContext(Assembly assembly)
        {
            ApplicationAssembly = assembly;

            var managedTypes = ApplicationAssembly.GetTypes().Where(type => type.IsDefined(typeof(ManagedAttribute), true));
            foreach (var managedType in managedTypes)
            {
                _managedObjectDictionry[managedType] = InstantiateManagedObject(managedType);
            }
        }

        public Assembly ApplicationAssembly { get; private set; }
        private readonly Dictionary<Type, object> _managedObjectDictionry = new Dictionary<Type, object>();

        public T GetManagedObject<T>() where T : class
        {
            return GetManagedObject(typeof(T)) as T;
        }

        public object GetManagedObject(Type targetType)
        {
            return targetType == typeof(ApplicationContext) ? this : GetManagedObjectImpl(targetType);
        }

        private object GetManagedObjectImpl(Type targetType)
        {
            return _managedObjectDictionry[targetType];
        }

        //TODO: 우선 Route를 제대로 할 수 있는지 테스트를해 보아야 하므로 하드코딩.
        private object InstantiateManagedObject(Type managedObjectType)
        {
            if (managedObjectType == typeof(ApplicationContext))
                return this;

            var autoWiredConstructor = managedObjectType.GetConstructors().FirstOrDefault(info => info.IsDefined(typeof(AutoWiredAttribute), true));
            if (autoWiredConstructor == null)
            {
                return _managedObjectDictionry[managedObjectType] = Activator.CreateInstance(managedObjectType);
            }

            var parameterInfos = autoWiredConstructor.GetParameters();
            var parameters = parameterInfos.Select(info => InstantiateManagedObject(info.ParameterType)).ToArray();
            return _managedObjectDictionry[managedObjectType] = autoWiredConstructor.Invoke(parameters);
        }
    }
}
