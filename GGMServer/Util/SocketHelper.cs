﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace GGMServer.Util
{
    public static class SocketHelper
    {
        public static Task<Socket> AcceptFromAsync(this Socket self)
        {
            return Task.Factory.FromAsync(self.BeginAccept, self.EndAccept, null);
        }

        public static Task<int> ReceiveFromAsync(this Socket self, byte[] buffer)
        {
            return Task.Factory.FromAsync(self.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, null, self), self.EndReceive);
        }

        public static Task<int> ReceiveFromAsync(this Socket self, byte[] buffer, int readSize)
        {
            return Task.Factory.FromAsync(self.BeginReceive(buffer, 0, readSize, SocketFlags.None, null, self), self.EndReceive);
        }

        public static Task SendFromAsync(this Socket self, byte[] buffer)
        {
            return Task.Factory.FromAsync(self.BeginSend(buffer, 0, buffer.Length, SocketFlags.None, null, self), self.EndSend);
        }

        public static Task SendFromAsync(this Socket self, IList<ArraySegment<byte>> buffers)
        {
            return Task.Factory.FromAsync(self.BeginSend(buffers, SocketFlags.None, null, self), self.EndSend);
        }

    }
}