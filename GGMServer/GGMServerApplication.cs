﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GGMServer.DI;
/////
namespace GGMServer
{
    public class GGMServerApplication
    {
        public static ApplicationContext Run(Type applicationClassType, string[] args)
        {
            return new GGMServerApplication(args).Run(applicationClassType);
        }

        private GGMServerApplication(string[] args)
        {
            Arguments = args;
        }

        //TODO: 현재는 string[]이지만 추후 객체화 할 것.
        public string[] Arguments { get; private set; }
        public Assembly Assembly { get; private set; }
        public ApplicationContext Context { get; private set; }

        public ApplicationContext Run(Type applicationClassType)
        {
            Assembly = applicationClassType.Assembly;
            Context = new ApplicationContext(Assembly);
//            throw new NotImplementedException();

            return Context;
        }
    }
}
